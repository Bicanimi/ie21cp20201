# Documentação do Projeto Relógio LCD sem RTC

## 1. Introdução

Descrição do Projeto: Trata-se de um relógio simples com base no arduino; este faz a 
contagem dos segundos, minutos e das horas, mas quando completa um ciclo de 24 
horas (1 dia), a contagem zera, além disso, conseguimos fazer com que toque uma 
especie de alarme as 06h:00min da manhã.


## 2. Objetivos

- Mostrar as horas.
- Alarmar as 6h da manhã.

## 3. Materiais utilizados

|Nome            |Quantidade          |Componente                   |
|---|---|---|
|U2              |1                   |Arduino Uno R3               |
|U1              |1                   |LCD 16x2                     |
|R2              |1                   |Resistor (220 Ôhn)           |
|PIEZO2          |1                   |Piezo2                       |
|Rpot3           |1                   |10KQ, Potenciômetro          |


## 4. Esquema Elétrico

Link do Tinkercad: https://www.tinkercad.com/things/5sSZRbQUG4Y



## 5. Código

```Cpp
	// biblioteca
	#include <LiquidCrystal.h>
	
	//definindo o pino do buzzer
	int buzzer = 9;
	
	//definindo o incio dos horarios
	int segundos = 0;
	int minutos = 0;
	int horas = 0;
	
	//definindo notas musicais
	#define sol 392
	#define la  440
	#define si  494
	#define do_1  263
	#define re  294
	#define mi  330
	#define fa  349
	
	// inicializacao dos pinos pela biblioteca
	LiquidCrystal lcd(7, 6, 5, 4, 3, 2);
	
	void setup()
	{
	  //iniciando o buzzer
	  pinMode(buzzer, OUTPUT);
	   
	  // definindo o tamanho do lcd com no numero de colunas e linhas
	  lcd.begin(16, 2);
	  Serial.begin(9600);
	}
	
	void loop()
	{
	  //organizando o cronometro para parar em 60
	  // pra acelerar e so multiplicar o millis
	  segundos = ((millis()) / 1000)-(minutos*60)-(horas*3600);
	 
	  //Onde vai aparecer a mensagem;
	  lcd.setCursor(0,0);
	 
	  // mensagem
	  lcd.print("Relogio");
	 
	  //onde vai aparecer a mensagem;
	  lcd.setCursor(0,1);
	 
	  //quando as horas forem igual a 24, o cronometro reinicia
	  if(horas > 23)
	  {
	   
	    horas = 0;
	    minutos = 0;
	    segundos = 0;
	 
	   //limpa tela
	   lcd.clear();
	  }
	 
	  // mostrando o cronometro em horas
	  lcd.print(horas);
	 
	  lcd.print(":");//separacao
	
	 
	  //quando os minutos forem igual a 60, o cronometro reinicia
	  if(minutos > 59)
	  {
	    minutos = 0;
	    segundos = 0;
	    horas = horas + 1;
	
	    //limpa tela  
	    lcd.clear();
	  }
	  // mostrando o cronometro em minutos
	  lcd.print(minutos);
	 
	  lcd.print(":");//separacao
	   
	  //quando os segundos forem igual a 60, o cronometro reinicia
	  if(segundos > 59)
	  {
	    minutos = minutos + 1;
	    segundos = 0;
	
	    //limpa tela  
	    lcd.clear();
	  }
	  //mostrando o cronometro em segundos
	  lcd.print(segundos);
	
	  //quando o relogio batar em 6 horas um despertador toca
	  if(horas == 6 && minutos == 0)
	  {
	    tone(buzzer,sol,500);
	    delay(500);
	    tone(buzzer,la,500);
	    delay(500);
	    tone(buzzer,si,500);
	    delay(500);
	    tone(buzzer,do_1,500);
	    delay(500);
	    tone(buzzer,re,500);
	    delay(500);
	    tone(buzzer,mi,500);
	    delay(500);
	    tone(buzzer,fa,500);
	    delay(500);
	   
	    //a musica para sozinha em 3,5 segundos
	    noTone(buzzer);
	  }
	  Serial.print(horas);
	  Serial.print(":");
	  Serial.print(minutos);
	  Serial.print(":");
	  Serial.println(segundos);
	}
```

## 6. Resultados

- Relógio funcional.
- Contador de segundos, minutos e horas.
- Alarme para tocar as 06h:00min da manhã.

O vídeo de demonstração pode ser visto em:
{{< youtube wGrQ15pnO2Y >}}


## 7. Desafios encontrados

-Criar um relógio sem a peça adequada (RTC). 

-Descobrir como limpar o lixo de memória em cada divisão de horário. 

-Terminar e reiniciar o ciclo do relógio quando encerrado o período de 24 horas. 

-Falta de conhecimento em eletrônica básica. 

-Desenvolver o site do projeto com o HUGO. 

-Achar comandos e tutoriais próprios do HUGO (Maior parte do conteúdo em inglês). 


## 8. Participantes

- JHONATTAN GABRIEL D. SANTANA;	
- ANNA BEATRIZ NASCIMENTO ;	
- GEOVANNA LIMA DE SÁ;	

-Projeto realizado como avaliação para a disciplina de “Introdução a engenharia” na UTFPR-PB, no periodo 2020.	

-Professor responsável: Jeferson José de Lima.	

-Contato: jefersonlima@utfpr.edu.br

