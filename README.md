# Introdução
•	Relógio:

=> possui como função mostrar as horas e tocar o alarme as 6h da manhã.

Veja a simulação do projeto clicando na imagem abaixo:

[![enter image description here](assets/EsquemaRelogioTinkerCad.jpeg)](https://www.tinkercad.com/things/5sSZRbQUG4Y) 


## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Anna Beatriz| [@Bicanimi](https://gitlab.com/Bicanimi)|
|Geovanna Sá |[@Geovanna_eng](https://gitlab.com/Geovanna_eng)|
|Jhonattan|[@Jhonattan_Gabriel_Santana](https://gitlab.com/Jhonattan_Gabriel_Santana)|

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  https://bicanimi.gitlab.io/ie21cp20201 



